////////////////////////////////////////// AUTHENTICATION VALID
function checkDataInput() {
    var userName = document.getElementById('username').value;

    var invalidUserNameFeedBack = document.getElementById('usernameInvalidFeedback');

    if (userName === "") {
        invalidUserNameFeedBack.innerHTML = 'Username is not empty!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (userName.length < 3 || userName.length > 30) {
        invalidUserNameFeedBack.innerHTML = 'Username is invalid!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else {
        event.target.classList.remove('is-invalid');
        displayButton();
    }
}

function checkDataEmail() {
    var email = document.getElementById('email').value;

    var invalidEmailFeedBack = document.getElementById('emailInvalidFeedback');

    if (email === "") {
        invalidEmailFeedBack.innerHTML = 'Email is not empty!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (email.length < 3) {
        invalidEmailFeedBack.innerHTML = 'Email is invalid!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (!isEmail(email)) {
        invalidEmailFeedBack.innerHTML = 'Email is invalid11!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else {
        event.target.classList.remove('is-invalid');
        displayButton();
    }
}

function isEmail(email) {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

function checkPassword() {
    var passWord = document.getElementById('password').value;

    var invalidPassWordFeedBack = document.getElementById('passwordInvalidFeedback');

    if (passWord === "") {
        invalidPassWordFeedBack.innerHTML = 'Password is not empty!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (passWord.length < 8 || passWord.length > 30) {
        invalidPassWordFeedBack.innerHTML = 'Password is invalid!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (!containPassword(passWord)) {
        invalidPassWordFeedBack.innerHTML =
            'Password is include one number, string, string uppercase and special character!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (passWord.includes('OR1=1') || passWord.includes('or1=1')) {
        invalidPassWordFeedBack.innerHTML = 'Password is violated security!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else {
        event.target.classList.remove('is-invalid');
        displayButton();
    }
}

function containPassword(passWord) {
    const regex = /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
    return regex.test(passWord);
}

function checkRePassword() {
    var rePassWord = document.getElementById('repassword').value;
    var passWord = document.getElementById('password').value;

    var invalidRePassWordFeedBack = document.getElementById('repasswordInvalidFeedback');

    if (rePassWord !== passWord) {
        invalidRePassWordFeedBack.innerHTML = 'Reinput please!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else {
        event.target.classList.remove('is-invalid');
        displayButton();
    }
}

function myFunction() {
    var email = document.getElementById('email').value;

    var invalidEmailFeedBack = document.getElementById('emailInvalidFeedback');

    if (email === "") {
        invalidEmailFeedBack.innerHTML = 'Email is not empty!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (email.length < 3) {
        invalidEmailFeedBack.innerHTML = 'Email is invalid!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else if (!isEmail(email)) {
        invalidEmailFeedBack.innerHTML = 'Email is invalid11!';
        event.target.classList.add('is-invalid');
        hideButton();
    } else {
        event.target.classList.remove('is-invalid');
        displayButton();
    }
}

////////////////////////////////////////// CONTENT CHECK VALID
function checkTitle() {
    var title = document.getElementById('title').value;

    var messageError = document.getElementById('checkValidTitle');
    if (title === "") {
        messageError.innerHTML = 'Title is not empty!';
        event.target.classList.add('is-invalid');
    } else if (title.length < 10 || title.length > 200) {
        messageError.innerHTML = 'Title is invalid!';
        event.target.classList.add('is-invalid');
    } else {
        event.target.classList.remove('is-invalid');
    }
}

function checkBrief() {
    var brief = document.getElementById('brief').value;

    var messageError = document.getElementById('checkValidBrief');

    if (brief === "") {
        messageError.innerHTML = 'Brief is not empty!';
        event.target.classList.add('is-invalid');
    } else if (brief.length < 30 || brief.length > 150) {
        messageError.innerHTML = 'Brief is invalid!';
        event.target.classList.add('is-invalid');
    } else {
        event.target.classList.remove('is-invalid');
    }
}

function checkContent() {
    var content = document.getElementById('content').value;

    var messageError = document.getElementById('checkValidContent');

    if (content === "") {
        messageError.innerHTML = 'Content is not empty!';
        event.target.classList.add('is-invalid');
    } else if (content.length < 50 || content.length > 1000) {
        messageError.innerHTML = 'Content is invalid!';
        event.target.classList.add('is-invalid');
    } else {
        event.target.classList.remove('is-invalid');
    }
}

function hideButton() {
    document.getElementById("btn-submit").disable = true;
    document.getElementById("btn-submit").style.backgroundColor = "#A1A1A1";
    document.getElementById("btn-submit").style.border = "2px solid #A1A1A1";
}

function displayButton() {
    document.getElementById("btn-submit").disable = false;
    document.getElementById("btn-submit").style.backgroundColor = "#5CB85C";
    document.getElementById("btn-submit").style.border = "2px solid #5CB85C";
}

$(function () {
    // function when click on register button 
    $('.register-btn').on('click', function (e) {
        e.preventDefault();

        var userName = $('#username').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var repassword = $('#repassword').val();

        var invalidEmailFeedBack = $('#emailInvalidFeedback').val();

        if (userName === "" || email === "" || password === "" || repassword === "") {
            invalidEmailFeedBack.innerHTML = 'Email is not empty!';
            event.target.classList.add('is-invalid');
            hideButton();
        } else {
            displayButton();
            var users = {
                "username": userName,
                "email": email,
                "password": password,
            }

            var userCheck = {
                "username": userName,
                "email": email,
            }

            // Check exists username & email
            $.ajax({
                method: 'POST',
                url: 'http://127.0.0.1:8000/api/auth/check-eu',
                data: userCheck,
                dataTypes: 'JSON',
                success: function (res) {
                    if (res.status === true && res.data > 0) {
                        alert('Email or username was exists!');
                    } else {
                        // Not exists username & email register account
                        $.ajax({
                            method: 'POST',
                            url: 'http://127.0.0.1:8000/api/auth/register-account',
                            data: users,
                            dataTypes: 'JSON',
                            success: function (res) {
                                if (res.status === true) {
                                    // redirect login
                                    window.location.href = "login.html";
                                }
                            },
                        })
                    }
                }
            })
        }
    })

    // function when click on login button 
    $('.login-btn').on('click', function (e) {
        e.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();

        if (email === "" && password === "") {
            alert('Vui lòng nhập đầy đủ thông tin!');
        } else {
            var users = {
                "email": email,
                "password": password
            }

            $.ajax({
                method: 'POST',
                url: 'http://127.0.0.1:8000/api/auth/check-ep',
                data: users,
                dataTypes: 'JSON',
                success: function (res) {
                    console.log(res)
                    if (res.status === true && res.data.length > 0) {
                        // login success
                        sessionStorage.setItem("id_user", res.data[0].id);
                        sessionStorage.setItem("email_user", email);
                        window.location.href = "list-content.html";
                    }
                }
            })
        }
    })

    // get data and inner from table
    var contents = [];

    buildTable(contents);

    $.ajax({
        method: 'GET',
        url: 'http://127.0.0.1:8000/api/get-content',
        dataTypes: 'JSON',
        success: function (res) {
            contents = res.data;
            buildTable(contents);
        },
        error: function (error) {
            alert(error);
        }
    })

    function buildTable(data) {
        var table = document.getElementById("myTable");

        for (var i = 0; i < data.length; i++) {
            var row = `<tr>
            <td>${data[i].id}</td>
            <td>${data[i].Title}</td>
            <td>${data[i].Brief}</td>
            <td>${moment(data[i].CreatedDate).format('DD-MM-YYYY HH:m')}</td>
        </tr>`;
            table.innerHTML += row;
        }
    }

    // logout account user
    $('#logout-data').click(function (e) {
        e.preventDefault();
        localStorage.removeItem('id_user');
        localStorage.removeItem('email_user');
        window.location.href = "login.html";
    })

    // add content data
    $('#add-content').on('click', function (e) {
        e.preventDefault();

        var title = $('#title').val();
        var brief = $('#brief').val();
        var content = $('#content').val();

        if (title === "" || brief === "" || content === "") {
            alert('Data is empty');
        } else {
            var contents = {
                "title": title,
                "brief": brief,
                "content": content
            }

            $.ajax({
                method: 'POST',
                url: 'http://127.0.0.1:8000/api/add-content',
                dataTypes: 'JSON',
                data: contents,
                success: function (res) {
                    window.location.href = "list-content.html";
                },
                error: function (error) {
                    alert(error);
                }
            })
        }
    })

    $('#formUser').ready(function () {
        var id = sessionStorage.getItem("id_user");
        var email = sessionStorage.getItem("email_user");

        var dataUser = {
            "id": id,
            "email": email
        }
        $.ajax({
            method: 'POST',
            url: 'http://127.0.0.1:8000/api/auth/get-data-fre',
            data: dataUser,
            dataTypes: 'JSON',
            success: function (res) {
                getDataOnFormEdit(res);
            },
        })
    });

    // get data from response to form input edit data
    function getDataOnFormEdit(res) {
        $('#firstName').val(res.data[0].FirstName);
        $('#lastName').val(res.data[0].LastName);
        $('#email').val(res.data[0].Email);
        $('#phone').val(res.data[0].Phone);
        $('#description').val(res.data[0].Description);
    }

    // update data 
    $('#submit-update').on('click', function (e) {
        e.preventDefault();
        var id = sessionStorage.getItem("id_user");

        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var phone = $('#phone').val();
        var description = $('#description').val();

        if (firstName === "" || lastName === "" || phone === "") {
            alert('Not empty data');
        } else {
            var userObject = {
                "firstName": firstName,
                "lastName": lastName,
                "phone": phone,
                "description": description
            }

            $.ajax({
                method: 'PUT',
                url: 'http://127.0.0.1:8000/api/auth/update-account/' + id,
                data: userObject,
                dataTypes: 'JSON',
                success: function (res) {
                    if(res.data > 0 && res.status === true) {
                        // location.reload();
                        alert('Update profile successful!');
                    }
                },
            })
        }
    })
})
